<?php

class Playlist {
    
    private $id;
    private $title;
    private $owner;
    private $privacity;
    
    function __construct($id, $title, $owner, $privacity) {
        $this->id = $id;
        $this->title = $title;
        $this->owner = $owner;
        $this->privacity = $privacity;
    }
    
    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getOwner() {
        return $this->owner;
    }

    function getPrivacity() {
        return $this->privacity;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setOwner($owner) {
        $this->owner = $owner;
    }

    function setPrivacity($privacity) {
        $this->privacity = $privacity;
    }


}
