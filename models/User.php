<?php


class User extends Model{
    
    protected static $table = "User";

    private $id;
    private $username;
    private $password;
    private $avatar;
    
    function __construct($id, $username, $password, $avatar = null) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->avatar = $avatar;
    }
    
    public function getMyVars(){
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getAvatar() {
        return $this->avatar;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setAvatar($avatar) {
        $this->avatar = $avatar;
    }


}
