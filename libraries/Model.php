<?php

class Model {
    
     protected static $table;
     
     public function save(){
        
         $values = $this->toArray();

         $db = new MySQLiManager("127.0.0.1", "root", "", "spotistiam");
         return $db->insert(static::$table,$values);
                 
     }
     
     public static function selectAll(){

         $db = new MySQLiManager("127.0.0.1", "root", "", "spotistiam");
         return $db->select("*",static::$table);
                 
     }
     
    public function toArray(){
        $arr = [];
        foreach ($this->getMyVars() as $key => $value) {
            $arr[$key] = $this->{"get".$key}();
        }
        return $arr;
    }

     
    
}
