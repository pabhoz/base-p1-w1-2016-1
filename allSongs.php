<?php

require 'lazyLoad.php';

$songs = Song::selectAll();

foreach ($songs as $key => $song) {
    echo $song["title"]." By ".$song["author"];
    echo "<audio src='".$song["file"]."' controls=true></audio>";
    echo "</br>";
}