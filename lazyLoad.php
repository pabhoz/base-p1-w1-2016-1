<?php

spl_autoload_register(function ($clase) {
    if(file_exists('./models/' . $clase . '.php')){
        include './models/' . $clase . '.php';
    }elseif(file_exists('./libraries/' . $clase . '.php')){
        include './libraries/' . $clase . '.php';
    }else{
        exit("Clase ".$class." no encontrada");
    }
});
